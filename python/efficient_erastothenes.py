#!/usr/bin/env python3

import numpy as np
import time

def sieve(n):
    flags = np.ones(n, dtype=bool)
    flags[0] = flags[1] = False
    for i in range(2, n):
        if flags[i]:
            flags[i*i::i] = False
    return np.flatnonzero(flags)

n = int(1e7)
start = time.time()
primes = sieve(n)
end = time.time()

print("Took " + "{:0.5f}".format(float(end-start)) + " seconds for first " + str(n) + " integers.")
print(primes)
