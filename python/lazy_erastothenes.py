#!/usr/bin/env python3

# sieve of erastothenes lazy implemenatation
# limited by max recursion depth ¯\_(ツ)_/¯

def naturals(n):
    while True:
        yield n
        n += 1

def sieve(s):
    p = next(s)
    yield p
    yield from sieve(i for i in s if i%p!=0)

try:
    for prime in sieve(naturals(2)):
        print(prime)
except RuntimeError as err:
    print(err)
